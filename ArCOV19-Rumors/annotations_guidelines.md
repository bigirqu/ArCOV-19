# ArCOV19-Rumors annotations guidelines

Given a claim X, label the tweet T as:
- Expressing same claim: if the main focused claim C in the tweet is restating or expressing or rephrasing X (i.e., C = X).
- Negating same claim: if the main focused claim C in the tweet is negating or denying X (i.e., C = not X).
- Other: if it is not one of the above (see examples below).

Please check the below examples:
- Claim: الوضع خطير في الصين: يطلقون النار على المرضى في الشوارع 

- Expressing same claim: انباء عن صدور اوامر من الحزب الشيوعي الحاكم في الصين بقتل المصابين بفايروس الكورونا الذين يخرجون من منازلهم... #كورونا #الصين_كورونا https://t.co/d1SNoR4gBE

- Negating same claim: 
الفيديو المتداول بعنوان «الصين بدأت بقتل المصابين بفيروس #كورونا» غير صحيح, والحقيقة أن الفيديو مفبرك وذلك عبر دمج ثلاثة مقاطع مختلفة وإستخدام فيديو آخر للشرطة وهي تتعامل مع كلب مسعور. https://t.co/6buJWYCqrQ https://t.co/6BfD8CWDcy 

- Examples of Other: 

  1. It is another relevant claim.
     - Claim: “فايروس كورونا المستجد "كوفيد19" ينتقل عن طريق النقود "الكاش”
     - Tweet: ”بنك الكويت المركزي: يدعو المواطنين والمقيمين الى عدم استخدام النقود الورقية لتقليل مخاطر الإصابة بفيروس كورونا، واستبدالها بالبطاقات الائتمانية! @CentralBank_KW @KuwaitiCM @KUWAIT_MOH @Drbaselalsabah https://t.co/VIZcrwBnyC” 
  2. Advice
     - Claim: “فايروس كورونا المستجد "كوفيد19" ينتقل عن طريق النقود "الكاش” 
	 - Tweet: ”#نصيحتي ١) إبتعدوا وخففوا من استعمال النقود الورقية واكتفوا ببطاقات المصارف. ٢)إبتعدوا مؤقتا عن المصافحة وغيره ٣)غسل الأيدي بعد كل مشوار ٤)أجلوا المشاوير المزدحمة التي لاداعي منها. #كورونا” 

  3. Question
     - Claim: “فايروس كورونا المستجد "كوفيد19" ينتقل عن طريق النقود "الكاش” 
     - Tweet: “سؤال : هوة فيروس كورونا ممكن ينتقل عن طريق تداول النقود ؟؟!”

  4. Opinion:
     - Claim: “فايروس كورونا المستجد "كوفيد19" ينتقل عن طريق النقود "الكاش” 
     - Tweet:  “أنا من رأيي انو كل المحلات يلغوا التعامل بالنقود يدًا بيد ولازم الجهات المختصة تفرض على جميع المحلات انو يكون الدفع عن طريق الصراف "كريدت" ويمنعوا منعا باتا الكاش منعا لنقل الأمراض لفتره محدوده . تتفقوا ؟!..”

  5. Have multiple claims:
     - Claim: “فايروس كورونا المستجد "كوفيد19" ينتقل عن طريق النقود "الكاش” 
     - Tweet: “ايران تعلن عن 15 حالة وفاة جديدة بسبب كورونا المستجد لترتفع الحصيلة الاجمالية الى 92 وفاة - تحذير .. فيروس "الكورونا" ينتقل عبر العملات النقدية قالت منظمة الصحة العالمية، إن فيروس "كورونا" قد ينتقل عبر لمس النقود وتداولها... ( يورو نيوز / الرؤية ) #يحدث_الان https://t.co/upcUmYd7z3”

  6. Sarcasm:
     - Claim: الشلولو من أكفأ مضادات الكورونا وغيرها من الفيروسات
     - Tweet: #مصر تقدم وجبة شلولو كمساعدات طبيه لامريكا لمواجهة #فيروس_كورونا 🤣

You should check the external links for the tweet. Even if the claim is not expressed explicitly in the tweet, if the external link is expressing or negating the claim then the tweet should be labeled as Expressing or Negating the claim.


