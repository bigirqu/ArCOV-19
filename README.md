# ArCOV-19

**Team:** [bigIR](https://sites.google.com/view/bigir) from Qatar University ([@bigIR_group](https://twitter.com/bigIR_group))

**Initial release date:** April 12, 2020

**Major update date:** February 23, 2021

**Paper:** [ArCOV-19: The First Arabic COVID-19 Twitter Dataset with Propagation Networks](https://camel.abudhabi.nyu.edu/WANLP-2021-Program/47_Paper.pdf)

This repository contains an **ongoing** collection of Arabic tweets associated with the novel Coronavirus COVID-19, 
collected starting from January 27, 2020.

ArCOV-19 is an Arabic COVID-19 Twitter dataset that covers the period from 27th of January till 5th of May 2021. ArCOV-19 is the first publicly-available Arabic Twitter dataset covering COVID-19 pandemic that includes about 3.2M tweets alongside the propagation networks of the most-popular subset of them (i.e., most-retweeted and-liked). The propagation networks include both retweets and conversational threads (i.e., threads of replies). ArCOV-19 is designed to enable research under several domains including natural language processing, information retrieval, and social computing, among others. Preliminary analysis shows that ArCOV-19 captures rising discussions associated with the first reported cases of the disease as they appeared in the Arab world. In addition to the source tweets and the propagation networks, we also release the search queries and the language-independent crawler used to collect the tweets to encourage the curation of similar datasets.

# ArCOV-19 Statistics (V5.0, May 11 2021)
| **Tweets Statistics** |  |  |
| ------ | ------ | ------ |
| Source Tweets | 3,140,158 |  |
| Posted by verified users | 586,287 | (18.67%)|
| Top Subset | 464,132 | (14.78%)|
| Retweets of Top subset | 7,925,821 |  |
| Replies of Top subset| 1,476,950 |  |

| **Users Statistics** |  |  |
| ------ | ------ | ------ |
| Unique | 795,654 |  |
| Verified | 5,981| (0.75%) |
| Average \# followers | 4,410 | |
| Average \# friends | 879 | |
| Average \# statuses |8,821 | |

# Contact us
- [Fatima Haouari](mailto:200159617@qu.edu.qa)
- [Maram Hasanain](mailto:maram.hasanain@qu.edu.qa)
- [Reem Suwaileh](mailto:rs081123@qu.edu.qa)
- [Dr. Tamer Elsayed](mailto:telsayed@qu.edu.qa)

# Acknowledgments
This work was made possible by NPRP grant\# NPRP 11S-1204-170060, GSRA grant\# GSRA5-1-0527-18082, and GSRA grant\# GSRA6-1-0611-19074 from the Qatar National Research Fund. The statements made herein are solely the responsibility of the authors. 
