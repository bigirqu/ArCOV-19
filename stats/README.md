This directory contains files listing most frequent hashtags, keywords and users in ArCOV-19 for the data up to 30th April 2020. It contains the following:

- **top100hashtags:** contains the top 100 frequent hashtags in ArCOV-19 and the frequency of each.
- **top100keywords:** contains the top 100 frequent keywords in ArCOV-19 and the frequency of each.
- **top100Users:** contains the top 100 tweeters in ArCOV-19 with the following information about each user:
  - user screen name: Twitter account handle (@screen_mame)
  - user name: tweeter full name as shown in her account
  - verified: a boolean value either TRUE or FALSE
  - number of tweets in ArCOV-19 posted by the user