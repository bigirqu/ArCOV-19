## Propogation Networks 
Directory of the replies and retweets for the top 1K tweets per day in ArCOV-19

### retweets 
The directory has one file per day including IDs of the top 1K tweets with their full set of retweets IDs in the following format:
   
    ##source tweet ID1##
    Retweet ID1
    Retweet ID2
    ..
    Retweet IDn
    ...
    ##source tweet ID1000##
    Retweet ID1
    Retweet ID2
    
    ..
    Retweet IDn


### replies
The directory has one file per day including IDs of the top 1K tweets with their full set of replies IDs in the following format:
    
    ##source tweet ID1##
    Reply ID1
    Reply ID2
    ..
    Reply IDn
    ...
    ##source tweet ID1000##
    Reply ID1
    Reply ID2
    ..
    Reply IDn