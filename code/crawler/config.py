# -*- coding: utf-8 -*-
"""
Created on Sun Dec 15 11:06:16 2019

@author: fhaou
"""
consumer_key = ""
consumer_secret = ""
access_token = ""
access_token_secret = ""


start_date="YYYY-MM-DD" #the crawler will start at startDate at 23:00 UTC time
start_time="23:30" #the crawler will start at startTime UTC time default is 23:30
searches_num=7 #the number of times(days) the crawler will perform a search with the set of queries.
 
topic_id="topic_id" #a unique ID for each topic
queries = ['user:@screen_name','query'] #'user:@screen_name' or/and 'query'
num_of_tweets=3200 #number of tweets to be returned for each query at the end of day
result_type="popular" #resultType:{popular,recent,mixed}
language='ar' 

debug="False"
daily_tweets_path="./%s_Tweets"%topic_id #Path for the tweets retrieved at the end of the 24hrs
log_path="./logs" #Path for the errors log files where exceptions are saved/Console output/logging output


