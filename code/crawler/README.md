Objective
=======================
Given a topic (represented in multiple queries), get popular tweets on that topic every day (for a period configured through the config file). 
The queries can be free text (hashtags, phrases, etc.) or user names (for this type, the query follows the pattern \'user:@screen_name>\').

How to run it
=======================
- Install all needed python libraries using the following command:
   `pip install -r requirements.txt`
- Set all needed parameters in the configuration file called "config.py" that is located in the same directory of the crawler (check the description of the parameters below).
- Run the crawler using the following command:
   `python Twitter_Crawler`

Parameters
=======================
The parameters of the crawler are all specified in a configuration file called "config.py". The crawler expects to find that file in the same directory as the crawler.
Here is the list of parameters:
- consumer\_key: Twitter API consumer key.
- consumer\_secret: Twitter API consumer secret.
- access\_token: Twitter API access token.
- access_token_secret: Twitter API access token secret.
- start\_date: the scheduled date for the crawler to start, in the format "YYYY-MM-DD".
- start\_time: the scheduled time in UTC for the crawler to start, default is "23:30".
- topic\_id: a unique id for the crawled topic.
- queries: an array of queries that represent the topic and will be used by the crawler to search Twitter, written in the format : ['query1','query2', \'user:@screen_name1\', \'user:@screen_name2', ...]
- searches_num: the number of times (days in our case) the crawler will perform a search with the set of queries. Set by default to 7.
- num\_of\_tweets: the number of tweets to be returned for each query at the end of day. Set by default to 3200.
- result_type: the type of retrieved tweets that can be either "popular", "recent", or "mixed". This is set by default to "popular" to get only popular tweets.
- language: language code of the tweets to be crawled.
- debug: set to "False" by default. If set to "True", debugging information will be saved to the logging file under the log directory (see LogPath).
- daily\_tweets\_path: by default it is in the following format: ./\<topic\_id\>\_Tweets. This directory will be created when the crawler starts. All saved tweets daily files will be in this directory.
- log_path: by default it is in the following format: ./LogFiles. This directory will be created when the crawler starts. The following will be saved in this directory:
  - \<topic\_id\>\_ConsoleOutput.txt: crawling analysis will be saved in this file such as: the number of tweets crawled for each query/number of tweets crawled in total for each day before/after deduplication etc.
  - \<topic\_id\>\_tc.log: all logging will be saved in this file including Info/Errors/Warnings and debugging if set to "True".

How it works
=======================
- When the time of the day hits start_time, the crawler issues a search request (through Twitter API) per query to get the most popular tweets for each query.
- After retrieving tweets for all queries, the crawler deduplicates the tweets (by id) and sort them by creation_time in ascending order before saving them as json objects.
- The final tweets set for each day will be saved in a separate file under dailyTweetsPath directory. The files saved will be in the following format \<topic_id\>\_\<crawlingDate\>\_\<crawlingTime\> (\<topic\_id\>\_\<YYYY\-MM\-DD\>\_\<HH\-MM\-SS\>)

How to setup the twitter app
========================
Once the credentials have been accepted...
- Access https://developer.twitter.com/ (login if necessary)
- Select "Apps" under the dropdown list with your name
- Create an app by filling the information required, you will get this immediately (for website, use CheckThat's official one)
- After creating an App click on generate keys at "keys and tokens".

Notes
=======================
- "Popular" tweets are retrieved by the API as defined by Twitter (we don’t really know the exact definition but we believe it is based on a mix of likes, retweets, impressions, etc.).
- The crawler is implemented to "reread" the queries right before it crawls for a new day. This allows the user to modify the list of queries of the topic at any time, which will be in effect starting the next crawl (scheduled at the end of the day).
