# -*- coding: utf-8 -*-
"""
Created on Thu Dec 12 20:15:02 2019

@author: fhaou
"""
 

from tweepy import OAuthHandler
import tweepy
import time
from datetime import timedelta
from datetime import datetime
import json
import config
import os
import threading
import gzip
from apscheduler.scheduler import Scheduler
import pytz
import numpy as np
import logging
from tzlocal import get_localzone
import sys
import imp
import re
import pandas as pd
from csv import writer

#parameters
startDate=config.start_date
start_time=config.start_time
searches_num=config.searches_num
topicID=config.topic_id #a unique ID for each topic
keywords = config.queries #keywords/hashtags to search Twitter
maxNum_of_tweets=config.num_of_tweets #number of tweets to be returned for each keyword each day
result_type=config.result_type #resultType:{popular,recent,mixed} 
language=config.language 
dailyTweetsPath=config.daily_tweets_path
LogPath=config.log_path
Debug=config.debug

#create the directory to save daily tweets on disk
try:
    os.mkdir(dailyTweetsPath)
except OSError:
    print ("The directory %s already exists" % dailyTweetsPath)
else:
    print ("Successfully created the directory %s " % dailyTweetsPath)
    
#create the directory to save errors log files
try:
    os.mkdir(LogPath)
except OSError:
    print ("The directory %s already exists" % LogPath)
else:
    print ("Successfully created the directory %s " % LogPath)


#Twitter API credentials
consumer_key = config.consumer_key
consumer_secret = config.consumer_secret
access_token = config.access_token
access_token_secret = config.access_token_secret
auth = OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
api = tweepy.API(auth)


#########################Saving tweets thread##################################
def save_tweets_thread(tweets,filename):
    print("Saving tweets in a parallel thread",flush=True) 
    print("********************************************************************************************************\n",flush=True)
    #Save to file
    start_time = time.time()
    for t in tweets:  
        try:
          with gzip.open(filename+'.gz', 'at',encoding='utf-8') as compressed_file:
               json.dump(t, compressed_file,default=str,ensure_ascii=False)
               compressed_file.write('\n')
         
        #handling exceptions at each tweet level
        except Exception as e:
         print('error while writing to the file/open a new one and save the retrieved tweets',flush=True)
         today=datetime.utcnow().strftime('%Y-%m-%d')
         current_time = datetime.utcnow().strftime("%H-%M-%S")
         filename=dailyTweetsPath+'/'+today+'_'+current_time+'_'+'24hrs' 
         with gzip.open(filename+'.gz', 'at',encoding='utf-8') as compressed_file:
               json.dump(t, compressed_file,default=str,ensure_ascii=False)
               compressed_file.write('\n')
         day_time=today+'_'+current_time
         print("type error: " + str(e),flush=True)
         f = open(LogPath+'/log_%s.txt'%day_time, 'w')
         f.write('The following exception occured - %s' % e )
         f.close() 
    print("time taken to save in disk is %s seconds ---" %(time.time() - start_time),flush=True)
    print("********************************************************************************************************\n",flush=True)

###############################################################################
#This is the search function
def get_tweets(filename,keyword, numOfTweets,resultType,language,since):

    #the search excludes retweets
    tweetsArray=[]
    crawlingTime=datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
    try:
       tweets=tweepy.Cursor(api.search,
                               q=keyword+' -filter:retweets',result_type=resultType,count=100,
                               tweet_mode='extended',
                               monitor_rate_limit=True, 
                               wait_on_rate_limit=True,
                               wait_on_rate_limit_notify=True,
                               lang=language,since=since).items(numOfTweets)
     
      
       print("Getting tweets for query: %s"%keyword,flush=True) 
#       print("********************************************************************************************************\n")
       start_time = time.time()
       for tweet in tweets:
          tweetsArray.append(tweet)
#       
       print("time taken to crawling for keyword %s is %s seconds/number of tweets returned %s" %(keyword,(time.time() - start_time),str(len(tweetsArray))),flush=True)
#       print("********************************************************************************************************\n")
        
    #handling exceptions at the search level     
    except Exception as e:
      today=datetime.utcnow().strftime('%Y-%m-%d')
      current_time = datetime.utcnow().strftime("%H-%M-%S")
      day_time=today+'_'+current_time
      print("type error: " + str(e),flush=True)
      f = open(LogPath+'/log_%s.txt'%day_time, 'w')
      f.write('The following exception occured - %s' % e)
      f.close()
       
    return tweetsArray,crawlingTime     

###############################################################################
#This function get tweets in atimeline of a user given its screenname
def get_users_tweets(filename,username,language,since):

    #the search excludes retweets
    tweetsArray=[]
    crawlingTime=datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
    try:
       tweets=tweepy.Cursor(api.user_timeline,
                               screen_name=username,
                               tweet_mode='extended',
                               monitor_rate_limit=True, 
                               wait_on_rate_limit=True,
                               wait_on_rate_limit_notify=True,
                               lang=language,since=since).items()
     
      
       print("Getting tweets for user: %s"%username,flush=True) 
#       print("********************************************************************************************************\n")
       start_time = time.time()
       for tweet in tweets:
           tweet_created_date=tweet.created_at.date().strftime('%Y-%m-%d')
           if tweet_created_date==since:
              tweetsArray.append(tweet)

             
#       
       print("time taken to crawling for user %s is %s seconds/number of tweets returned %s" %(username,(time.time() - start_time),str(len(tweetsArray))),flush=True)
#       print("********************************************************************************************************\n")
        
    #handling exceptions at the search level     
    except Exception as e:
      today=datetime.utcnow().strftime('%Y-%m-%d')
      current_time = datetime.utcnow().strftime("%H-%M-%S")
      day_time=today+'_'+current_time
      print("type error: " + str(e),flush=True)
      f = open(LogPath+'/log_%s.txt'%day_time, 'w')
      f.write('The following exception occured - %s' % e)
      f.close()
       
    return tweetsArray,crawlingTime     

########################################################################################
def CountFrequency(my_list): 
    # Creating an empty dictionary  
    freq = {} 
    for item in my_list: 
        if (item in freq): 
            freq[item] += 1
        else: 
            freq[item] = 1
    return freq


#####################################################################################
#A function to write to write an array to csv
def append_list_as_row(file_name, list_of_elem):
    # Open file in append mode
    with open(file_name, 'a', newline='') as write_obj:
        # Create a writer object from csv module
        csv_writer = writer(write_obj)
        # Add contents of list as last row in the csv file
        csv_writer.writerow(list_of_elem)
#####################################################################################
#load search queries
def get_keywords():
    imp.reload(config)
    return config.queries
######################################################################################
def get_tweets_every_24hours():
    #load search queries
    keywords=get_keywords()
    ######statistics#########
    tweetsNo=[]
    #########################
    print('Queries are: ',keywords)
    today=datetime.utcnow().strftime('%Y-%m-%d')
    current_time = datetime.utcnow().strftime("%H-%M-%S")
    statisticsDate=['Date: '+today]
    append_list_as_row(LogPath+'/%s_statistics_tc.csv'%topicID, statisticsDate)
    filename=dailyTweetsPath+'/'+topicID+'_'+today+'_'+current_time 
    print("current time : {}".format(time.ctime()),flush=True)
    seenTweetsIds=[]
    tweetsArray=[]         
    for keyword in keywords:
        queryTweetsArray=[]
        #check if the query is a username
        pattern = re.compile("user:@[A-Za-z0-9_]+")
        match=bool(pattern.fullmatch(keyword))
        #if the query is a username get the user timeline
        if match:
           username=keyword.split(':')[1]
           tweets,crawlingTime=get_users_tweets(filename,username,language,since=today)  
        #else search Twitter with the query
        else:
           tweets,crawlingTime=get_tweets(filename,keyword,maxNum_of_tweets,result_type,language,since=today)  
        try:
         for tweet in tweets:
            t=tweet._json
            t['topicID'] = topicID
            t['crawlingTime']=crawlingTime
            queryTweetsArray.append(t)
            tweetsArray.append(t)
            seenTweetsIds.append(t["id"])
         tweetsNo.append(len(queryTweetsArray))
        except Exception as e:
             print("error",e,flush=True)

    #after getting all tweets of all queries deduplicate tweets by keeping the recent tweet
    freq=CountFrequency(seenTweetsIds)
    deduplicatedTweetsIds=[]
    deduplicatedTweets=[]
    for index,elem in enumerate(seenTweetsIds): 
       for key, value in freq.items():
           if key==elem:
              if value>1:
                 freq[key]-=1
              elif value==1:
                 deduplicatedTweetsIds.append(elem)
                 deduplicatedTweets.append(tweetsArray[index])
    #sort tweets by creation_time            
    sortingIndices=np.argsort(deduplicatedTweetsIds)
    sortedTweets=[]
    for i in sortingIndices:
        sortedTweets.append(deduplicatedTweets[i])
    print("Total number of tweets crawled is:",len(tweetsArray),flush=True)
    print("Total number of tweets after deduplication is:",len(sortedTweets),flush=True)
    ######################statistics##############################################
    statistics = {'Query': keywords, 
        'Tweets number':tweetsNo}
    statisticsDF = pd.DataFrame(statistics, columns = ['Query','Tweets number'])
    statisticsDF.to_csv(LogPath+'/%s_statistics_tc.csv'%topicID,mode='a',index=False,encoding='utf-8-sig')
    TotalStatistics=['Tweets after deduplication',len(sortedTweets)]
    EmptyLine=['']
    append_list_as_row(LogPath+'/%s_statistics_tc.csv'%topicID, TotalStatistics)
    append_list_as_row(LogPath+'/%s_statistics_tc.csv'%topicID, EmptyLine)
    ###############################################################################
    x = threading.Thread(target=save_tweets_thread, args=(sortedTweets,filename))
    x.start()     
            

#Close results writing files and terminate scheduler neatly
def terminate_scheduler(thesched, should_exit=False):
    if should_exit:
       thesched.shutdown(wait=False) #wait till all tasks are done then close

            
if __name__ == "__main__":
    
    statisticsDF=pd.DataFrame()
    statisticsDF.to_csv(LogPath+'/%s_statistics_tc.csv'%topicID,mode='a',index=False,encoding='utf-8-sig')
    
    print("Console output will be printed to %s/%s_ConsoleOutput.txt "%(LogPath,topicID))
    print("Info/Warnings/Errors/Critical messages will be logged to %s/%s_tc.log"%(LogPath,topicID))

    #print all the console output to a file
    sys.stdout = open(LogPath+'/%s_ConsoleOutput.txt'%topicID, 'w',encoding="utf-8")
    #log Info
    if Debug=="True":
       loggingLevel=logging.DEBUG
    elif Debug=="False":
       loggingLevel=logging.INFO
    else:
       print("please modify the Debug parameter in CLEF_Params.py/True to log debugging or False otherwise" )
#    logging.basicConfig(level=loggingLevel,filename=LogPath+'/FullLogging.log',filemode='w', format='%(name)s - %(levelname)s - %(message)s')
    handler = logging.FileHandler(LogPath+'/%s_tc.log'%topicID, 'w', 'utf-8')
    formatter = logging.Formatter('%(name)s - %(levelname)s - %(message)s') 
    handler.setFormatter(formatter) # Pass handler as a parameter, not assign
    logger = logging.getLogger()
    logger.setLevel(loggingLevel)
    logger.addHandler(handler)

    logger.debug('This is a debug message')
    logger.info('This is an info message')
    logger.warning('This is a warning message')
    logger.error('This is an error message')
    logger.critical('This is a critical message')
    
    startTime=start_time
    startTimeDate= datetime.strptime(startDate+' '+startTime, '%Y-%m-%d %H:%M')
    
    #convert the startTime to the machine time
    tz = get_localzone()
    local = pytz.timezone ("utc")
    naive = startTimeDate
    local_dt = local.localize(naive, is_dst=None)
    utc_dt = local_dt.astimezone(tz)
    utc_dt=utc_dt.replace(tzinfo=None)
    #instantiate the scheduler and add a job that runs every 24 hours
    sched = Scheduler({'apscheduler.timezone': 'UTC','apscheduler.standalone': True})
    sched.add_interval_job(get_tweets_every_24hours, hours=24, start_date=utc_dt,max_runs=searches_num,max_instances=7)

    shutdown_date = utc_dt + timedelta(days=(searches_num-1),minutes=10)
    sched.add_date_job(terminate_scheduler, date=shutdown_date, args=[sched, {"should_exit": True}]) # add job function for terminating scheduler

    try:
     sched.start()
    except Exception as e:
     print("An error occured with the scheduler: %s"%e)

    
    